$(function() {
    toastr.options = {
    "debug": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
    }

    $("#loginbutton").click(function() {
        var uname = $("#loguname").val()
        var passw = $("#logpassw").val()
        $.ajax({
            url: "/auth/login",
            datatype: "json",
            contentType: "application/json",
            type: "POST",
            headers: {
        		"X-CSRFToken": getCookie("csrftoken")
                },
            data: JSON.stringify({
                "username": uname,
                "password": passw
            }),
            success: function(json){
                console.log(json)
                $(location).attr('href', '/')
            },
            error: function(json) {
                toastr.error("Error logging in")
            }
        });
    });
})


//======OTHER FUNCTIONS======//
/*
Generate CSRF token for POST requests
*/
function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
 }
