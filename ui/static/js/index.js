/*
Turn all the tables into DataTables on page load
*/
$(document).ready( function () {
    $('#runningJobsTable').DataTable();
    $('#completedJobsTable').DataTable();
    $('#nextJobsTable').DataTable();

    $('#confJobsTable').DataTable();
    $('#usersTable').DataTable();
} );

/*
On page load, show th ehome container and get information
to put into the tables
*/
$(function() {
    $("#homeCont").show()
    GetRunningJobs()
    GetTenCompletedJobs()
    GetTenNextJobs()
    ChartData()
    setInterval(function() {
        $("#runningJobsTableBody tr").remove();
        $("#completedJobsTableBody tr").remove();
        $("#nextJobsTableBody tr").remove();
        GetRunningJobs()
        GetTenCompletedJobs()
        GetTenNextJobs()
        ChartData()
    },60000);
    toastr.options = {
    "debug": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
    }
})

//======HOME======//
/*
Hide all containers and then show the home containers
when clicking the Home button
*/
function homeInit() {
    $(".data-container").hide()
    $("#homeCont").show()
}
/*
Get a list of all the in progress jobs
then create table rows in the runningjobstable
*/
function GetRunningJobs() {
    $.ajax({
        url: "/jobs/progress",
        dataType: "json",
        type: "GET",
        success: function(json) {
            //console.log(json);
            $.each(json, function(i, item) {
                var $tr = $("<tr>").append(
                    $("<td>").text(item.JobName),
                    $("<td>").text(item.Date + " " + item.Time),
                ).appendTo("#runningJobsTable");
            });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

/*
Get the 10 most recently completed jobs
Add these to rows in the completedjobstable
*/
function GetTenCompletedJobs() {
    $.ajax({
        url: "/jobs/complete?limit=10",
        dataType: "json",
        type: "GET",
        success: function(json) {
            //console.log(json)
            $.each(json, function(i, item) {
                if (item.ExitStatus == 0) {
                    statuscolor = "#a6fc7b"
                } else {
                    statuscolor = "#ff5462"
                }
                var $tr = $("<tr bgcolor='" + statuscolor +"'>").append(
                    $("<td>").text(item.JobID),
                    $("<td>").text(item.JobName),
                    $("<td>").text(item.Date + " " + item.Time),
                    $("<td>").text(item.ExitStatus)
                ).appendTo("#completedJobsTable")
            });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

/*
Get the next 10 scheduled jobs
Add these to rows in the nextjobstable
*/
function GetTenNextJobs() {
    $.ajax({
        url: "/jobs/scheduled?limit=10",
        dataType: "json",
        type: "GET",
        success: function(json) {
            $.each(json, function(i, item) {
                var $tr = $("<tr>").append(
                    $("<td>").text(item.ID),
                    $("<td>").text(item.JobName),
                    $("<td>").text(item.Date + " " + item.Time),
                    $("<td>").text(item.Command)
                ).appendTo("#nextJobsTable")
            });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

function ChartData() {
    $.ajax({
        url: "/jobs/complete",
        dataType: "json",
        type: "GET",
        timeout: 3000,
        success: function(json) {
            SuccessFailChart(json);
        },
        error: function(xhr, errmsg, err) {
    		console.log("Error building chart: " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr.error(xhr.status + ": " + xhr.responseText);
            return false;
        }
    });
}
function SuccessFailChart(json) {
    var suc = 0
    var fai = 0
    $.each(json, function(i, item) {
        if (item.ExitStatus == 0) {
            suc++;
        } else {
            fai++
        }
    })
    var chart =  new CanvasJS.Chart("chartContainer",
    {
    	title: {
    		text: "Succeeded/Failed Jobs"
    	},
    	animationEnabled: false,
    	data: [{
    		type: "pie",
    		startAngle: 40,
    		toolTipContent: "<b>{label}</b>: {y}",
    		showInLegend: "true",
    		legendText: "{label}",
    		dataPoints: [
    			{ y: suc, label: "Success"},
    			{ y: fai, label: "Fail"}
    		]
        }]
    });
    chart.render()
}

//======JOBS======//
/*
Show just the jobs container then get job information
*/
function jobsInit() {
    $(".data-container").hide()
    $("#confJobsTableBody tr").remove();
    $("#schedJobsTableBody tr").remove();
    $("#jobsCont").show()
    GetSchedJobs()
    GetConfiguredJobs()
}

/*
Hide/Show the tables based on what button is clicked
*/
function Schedbutton() {
    $("#schedJobs").show()
    $("#confJobs").hide()
}

function Confbutton() {
    $("#confJobs").show()
    $("#schedJobs").hide()
}

function JobsHide() {
    $("#schedJobs").hide()
    $("#confJobs").hide()
}

/*
Api call to get all scheduled jobs
*/
function GetSchedJobs() {
    /*$('#schedJobsTable').DataTable({
        "ajax": {
            "url": "/jobs/scheduled"
        },
        "columns": [
            {title: "ID"},
            {title: "JobName"},
            {title: "Date" + "Time"},
            {title: "Command"},
            {title: "JobStatus"},
            {title: "Groups"},
        ],
    });*/

    $.ajax({
        url: "/jobs/scheduled?limit=100",
        dataType: "json",
        type: "GET",
        success: function(json) {
        $('#schedJobsTable').DataTable({
            "ajax": {
                "url": "/jobs/scheduled?limit=100"
            },
            "columns": [
                {title: json.ID},
                {title: json.JobName},
                {title: json.Date + json.Time},
                {title: json.Command},
                {title: json.JobStatus},
                {title: json.Groups},
            ],
        });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

/*
Api call to get all configured jobs
*/
function GetConfiguredJobs() {
    $.ajax({
        url: "/jobs",
        dataType: "json",
        type: "GET",
        success: function(json) {
            $.each(json, function(i, item) {
                if (item.RepMinutes != 0) {
                    repeat = item.RepMinutes + " Minutes"
                } else if (item.RepHours != 0) {
                    repeat = item.RepHours + " Hours"
                } else if (item.RepDays != 0) {
                    repeat = item.RepDays + " Days"
                } else if (item.RepWeeks != 0) {
                    repeat = item.RepWeeks + " Weeks"
                } else if (item.RepMonths != 0) {
                    repeat = item.RepMonths + " Months"
                } else {
                    repeat = "1 Days"
                }
                var $tr = $("<tr>").append(
                    $("<td>").text(item.ID),
                    $("<td>").text(item.JobName),
                    $("<td>").text(item.Command),
                    $("<td>").text(item.Hour + ":" + item.Minute),
                    $("<td>").text(item.EndHour + ":" + item.EndMinute),
                    $("<td>").text(repeat),
                    $("<td>").text(item.Groups),
                ).appendTo("#confJobsTable")
            });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

//======SCHEDULE======//

//======USERS======//
/*
Hide all containers and then show the useres container
when clicking the Users button
*/
function usersInit() {
    $(".data-container").hide()
    $("#usersCont").show()
    $("#usersTableBody tr").remove()
    GetUsersList()
}

function GetUsersList() {
    $.ajax({
        url: "/users",
        dataType: "json",
        type: "GET",
        success: function(json) {
            $.each(json, function(i, item) {
                var $tr = $("<tr>").append(
                    $("<td>").text(item.ID),
                    $("<td>").text(item.Username),
                    $("<td>").text(item.FirstName),
                    $("<td>").text(item.SecondName)
                ).appendTo("#usersTable")
            });
        },
        error: function(xhr, errmsg, err) {
            console.log("error - " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr["error"](xhr.status + ": " + xhr.responseText);
        }
    });
}

function addUser() {
    console.log("Adding user")
    var fname = $("#addfname").val()
    var sname = $("#addsname").val()
    var uname = $("#adduname").val()
    var pass1 = $("#addpass1").val()
    var pass2 = $("#addpass2").val()

    if (pass1 != pass2) {
        toastr.error("Passwords did not match")
    } else {
        $.ajax({
            url: "/users/add",
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            headers: {
        		"X-CSRFToken": getCookie("csrftoken")
                },
            data: JSON.stringify({
                "firstname": fname,
                "secondname": sname,
                "username": uname,
                "password": pass1
            }),
            success: function() {
                $('#addUserModal').modal('hide');
                toastr.info("User " + uname + " Created")
            },
            error: function(json) {
                toastr.error("Failed to create user " + uname)
                console.log(json)
            }
        });
    }
}

//======OTHER FUNCTIONS======//
/*
Generate CSRF token for POST requests
*/
function getCookie(c_name)
{
    if (document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

/*
Handle code execution for hiding/showing the side menu
*/
function sidetog() {
    $("#sidebar").toggleClass("active");
    $("#content").toggleClass("active");
    $("#sidebarCollapse").toggleClass("fa-compress fa-expand");
}

/*
Logout function
*/
function logout() {
    $.ajax({
        url: "/auth/logout",
        dataType: "json",
        type: "GET",
        success: function(json) {
            $(location).attr('href', '/login.html')
            toastr.info("Logout successful")
        },
        error: function(xhr, errmsg, err) {
            console.log("Error logging out: " + xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            toastr.error(xhr.status + ": " + xhr.responseText);
        }
    });
}
