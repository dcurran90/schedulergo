FROM golang:1.11 as build
WORKDIR /schedulergo
COPY api /schedulergo/api
COPY ui /schedulergo/ui
RUN apt-get update && apt-get upgrade -y \
  && mkdir bin/ \
  && go get github.com/rakyll/statik \
  github.com/BurntSushi/toml \
  github.com/go-sql-driver/mysql \
  github.com/gorilla/handlers \
  github.com/gorilla/mux \
  github.com/streadway/amqp \
  golang.org/x/crypto/bcrypt \
  && statik -src=/schedulergo/ui \
  && CGO_ENABLED=0 GOOS=linux go build -tags netgo -a -v -o bin/schedulergo api/*

FROM alpine:latest
COPY --from=build /schedulergo/bin/schedulergo /schedulergo
RUN chmod +x /schedulergo
EXPOSE 8080
ENTRYPOINT ["/schedulergo"]
