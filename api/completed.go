package main

import (
    "bytes"
    "encoding/json"
    "fmt"
    "net/http"
    "io"
    "io/ioutil"

    _ "github.com/go-sql-driver/mysql"
    "github.com/gorilla/mux"
    "github.com/streadway/amqp"
)

type Completed struct{
    ID          int     `json: "id"`
    JobID       string  `json: "jobid"`
    JobName     string  `json: "jobname"`
    Time        string  `json: "time"`
    Date        string  `json: "date"`
    ExitStatus  int     `json: "exitstatus"`
    Message     string  `json: "message"`
    Errors      string  `json: "errors"`
}

/*
GET
List all the completed jobs
https://myserver.com:8080/jobs/complete
    * limit - return x number of records
    * f - comma seperated fields to select
Returns JSON:
[{
	"ID": 164,
	"JobID": 2,
	"JobName": "That job",
	"Time": "12:30:00",
	"Date": "2018-02-21",
	"ExitStatus": 0,
	"Agent": "127.0.0.1"
}]
*/
func JobsCompletedHandler(w http.ResponseWriter, r *http.Request){
    var jobslist []Completed
    var fields string
    selectq, ok := r.URL.Query()["f"]
    if ok {
        fields = selectq[0]
    } else {
        fields = "*"
    }
    querystr := fmt.Sprintf("SELECT %s from completed_jobs", fields)
    keys, ok := r.URL.Query()["limit"]
    if ok {
        querystr = fmt.Sprintf(querystr + " LIMIT %s", keys[0])
    }
    rows, err := db.Query(querystr)
    chkErr(err)


    for rows.Next() {
        var ID, ExitStatus int
        var JobID, JobName, Agent, Time, Date, Message, Errors string
        err = rows.Scan(&ID, &JobID, &JobName, &Date, &Time, &ExitStatus, &Agent, &Message, &Errors)
        jobslist = append(jobslist, Completed{ID, JobID, JobName, Date, Time, ExitStatus, Message, Errors})
        chkErr(err)
    }
    jsonbytes, err :=json.Marshal(&jobslist)
    chkErr(err)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)
}

/*
POST
https://myserver.com:8080/jobs/complete/add
Adds job to the completed_jobs table
Fields required:
    * jobid
    * jobname
    * time
    * date
    * exitstatus
    * agent
Fields not required:
    * message
    * errors
*/
func JobsCompletedAddHandler(w http.ResponseWriter, r *http.Request){
    var compjob Completed
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    err = r.Body.Close()
    chkErr(err)

    err = json.Unmarshal(body, &compjob)
    if err != nil {
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(442)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }
    // Remove from in progress first
    progurl := fmt.Sprintf("https://%s:%d/jobs/progress/delete/%d?token=%s", localconn, bindport, compjob.JobID, GetSystemToken())
    _, err = client.Get(progurl)
    chkErr(err)

    // then mark it as complete
    t := CompInsert(compjob)
    if t != nil {
        WebResult(w, http.StatusInternalServerError, "Failed to complete job")
    } else {
        WebResult(w, http.StatusOK, "Job Completed")
    }
}

/*
GET
https://myserver.com:8080/jobs/complete/delete/{jobid}
Delete jobs from completed_jobs
*/
func JobsCompletedDelHandler(w http.ResponseWriter, r *http.Request){
    stmt, err := db.Prepare("DELETE FROM completed_jobs WHERE ID=?")
    chkErr(err)

    vars := mux.Vars(r)
    jobid := vars["jobid"]

    res, err := stmt.Exec(jobid)
    chkErr(err)
    aff, err := res.RowsAffected()
    chkErr(err)
    fmt.Fprintln(w, aff)
}

/*
Carry out the MySQL insert
*/
func CompInsert(job Completed) error {
    stmt, err := db.Prepare("INSERT INTO completed_jobs" +
        "(JobID, JobName, Time, Date, ExitStatus, Agent, Message, Errors)" +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)")
    chkErr(err)
    defer stmt.Close()
    _, err = stmt.Exec(&job.JobID, &job.JobName, &job.Time, &job.Date, &job.ExitStatus, &job.Message, &job.Errors)
    chkErr(err)
    return err
}

func MarkComplete(msgs <-chan amqp.Delivery) {
    systoken := GetSystemToken()
    for m := range msgs {
        var jobret JobReturn
        err = json.Unmarshal(m.Body, &jobret)
        chkErr(err)
        jobid := jobret.Id
        jdetails := GetJobDetails(jobid, "inprogress")
        /*==================================
                MARK AS COMPLETE
        ==================================*/
        // hit the API endpoint to mark a job as complete
        Compdate, Comptime := GetDateTime()
        compurl := fmt.Sprintf("https://%s:%d/jobs/complete/add?token=%s", localconn, bindport, systoken)
        compdata := Completed{0, jdetails.Id, jdetails.JobName, Comptime, Compdate, jobret.ExitStatus, jobret.Message, jobret.Errors}
        compjson, err := json.Marshal(compdata)
        chkErr(err)
        compreq, err := http.NewRequest("POST", compurl, bytes.NewBuffer(compjson))
        chkErr(err)
        compreq.Header.Set("Content-Type", "application/json")
        _, err = client.Do(compreq)
        chkErr(err)
    }
}
