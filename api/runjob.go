package main

import (
    "database/sql"
    "fmt"
    "net/http"
)

type CacheStruct struct {
    ID    int     `json: id`
    Date  string  `json: date`
    Time  string  `json: time`

}

/*
POST
Force a refresh of the job cache so that new jobs are picked up for running
https://myserver.com:8080/cache/build
Returns JSON
{
  status: 200,
  message: "Cache rebuilt"
}
*/
func BuildCacheHandler(w http.ResponseWriter, r *http.Request) {
    forceRefresh := fmt.Sprintf("UPDATE %s SET Refresh = 1", jobcachetable)
    _, err := db.Query(forceRefresh)
    webErr(w, err)
    go func() {
        /*
        Check that refresh now equals 1 before carrying on
        so we don't have two caches trying to run at the same time
        Then trigger a cache refresh by calling BuildCache()
        */
        var Refresh int
        for Refresh == 1 {
            refchk := CheckRunRefresh()
            defer refchk.Close()
            for refchk.Next() {
                var Refresh int
                err = refchk.Scan(&Refresh)
                chkErr(err)
            }
        }
        jobcache, err := BuildCache()
        chkErr(err)
        CheckRun(jobcache)
    } ()
    WebResult(w, http.StatusOK, "Cache rebuilt")
}

func BuildCache() ([]CacheStruct, error) {
    var cachelist []CacheStruct
    date, time := GetDateTime()
    querystr := fmt.Sprintf("SELECT ID, JobName, Time FROM %s WHERE Date = '%s'", schedtable, date)
    fmt.Println(querystr)
    rows, err := db.Query(querystr)
    chkErr(err)

    defer rows.Close()
    for rows.Next() {
        var ID int
        var Time, Date string
        err = rows.Scan(&ID, &Time, &Date)
        cachelist = append(cachelist, CacheStruct{ID, Time, Date})
        chkErr(err)
    }
    fmt.Println("Cache built")
    builtquer := fmt.Sprintf("UPDATE %s SET Built = '%s %s', Refresh = 0", jobcachetable, date, time)
    fmt.Println(builtquer)
    _, err = db.Query(builtquer)
    chkErr(err)
    return cachelist, err
}


func CheckRun(jobcache []CacheStruct) {
    fmt.Println("Checking for jobs to run")
    systoken := GetSystemToken()
    for {
        _, time := GetDateTime()

        for j := range jobcache {
            if jobcache[j].Time == time {
                fmt.Println(fmt.Sprintf("Running Job %d", jobcache[j].ID))
                runurl := fmt.Sprintf("https://%s:%d/jobs/run/%d?token=%s", localconn, bindport, jobcache[j].ID, systoken)
                _, err = client.Get(runurl)
                chkErr(err)
            }
        }
        /*
        Check if a refresh needs to happen
        If it is required then this for loop breaks,
        ready to be re-run
        */
        refchk := CheckRunRefresh()
        defer refchk.Close()
        var Refresh int
        for refchk.Next() {
            err = refchk.Scan(&Refresh)
            chkErr(err)
        }
        if Refresh == 1 {
            break
        }
    }
}

/*
Fucntion to check if Refresh is set to 1
If it is then the CheckRun() function should stop as a
cache refresh has been triggered
*/
func CheckRunRefresh() *sql.Rows {
    quer := fmt.Sprintf("SELECT Refresh FROM %s WHERE Refresh=1 LIMIT 1", jobcachetable)
    rows, err := db.Query(quer)
    chkErr(err)
    return rows
}
