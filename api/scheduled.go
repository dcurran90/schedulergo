package main

import (
    "encoding/json"
    "fmt"
    "net/http"
    //"io"
    //"io/ioutil"

    _ "github.com/go-sql-driver/mysql"
    //"github.com/gorilla/mux"
)

type Schedjob struct {
    ID        int     `json: "id"`
    JobName   string  `json: "jobname"`
    Time      string  `json: "time"`
    Date      string  `json: "date"`
    Command   string  `json: "command"`
    JobStatus string  `json: "jobstatus"`
    Groups    string  `json: "groups"`
}

/*
GET
List the configured users.
https://myserver.com:8080/jobs.scheduled
    * limit - return x number of records
    * job - search for specific job name
    * f - comma seperated fields to select
returns JSON:
[{
    "id": int,
    "jobname": "string",
    "time": "string",
    "date": "string",
    "command": "string",
    "jobstatus": "string",
    "groups": "string"
}]
*/
func JobsScheduledHandler(w http.ResponseWriter, r *http.Request) {
    var schedlist []Schedjob
    var fields string
    // check the incomoing URL parameters for search refinement
    selectq, ok := r.URL.Query()["f"]
    if ok {
        fields = selectq[0]
    } else {
        fields = "*"
    }
    querystr := fmt.Sprintf("SELECT " + fields +" from scheduled_jobs")

    limit, ok := r.URL.Query()["limit"]
    if ok {
        querystr = fmt.Sprintf(querystr + " LIMIT %s", limit[0])
    }
    search, ok := r.URL.Query()["job"]
    if ok {
        querystr = fmt.Sprintf(querystr + " WHERE JobName=%s", search[0])
    }

    rows, err := db.Query(querystr)
    chkErr(err)
    defer rows.Close()
    for rows.Next() {
        var ID int
        var JobName, Time, Date, Command, JobStatus, Groups string
        err = rows.Scan(&ID, &JobName, &Time, &Date, &Command, &JobStatus, &Groups)
        schedlist = append(schedlist, Schedjob{ID, JobName, Time, Date, Command, JobStatus, Groups})
        chkErr(err)
    }
    jsonbytes, err :=json.Marshal(&schedlist)
    chkErr(err)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)
}
