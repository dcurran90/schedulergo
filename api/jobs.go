package main

import (
    "bytes"
    "encoding/json"
    "fmt"
    "net/http"
    "io"
    "io/ioutil"
    "reflect"

    _ "github.com/go-sql-driver/mysql"
    "github.com/gorilla/mux"
    "github.com/streadway/amqp"
)

// The struct to hold information about a given job
type Job struct {
    ID          string  `json: "id"`
    Command     string  `json: "command"`
    JobName     string  `json: "jobname"`
    Hour        string  `json: "hour"`
    Minute      string  `json: "minute"`
    RepMode     string  `json: "repmode"`
    RepIter     string  `json: "repiter"`
    DayofWeek   string  `json: "dayofweek"`
    IterinMonth string  `json: "iterinmonth"`
    EndHour     string  `json: "endhour"`
    EndMinute   string  `json: "endminute"`
    MaxIter     string  `json: "maxiter"`
    DelayStart  string  `json: "delaystart"`
    Groups      string  `json: "groups"`
}

type JobRun struct {
    Id          string  `json: "id"`
    JobName     string  `json: "name"`
    Command     string  `json: "command"`
    Groups      string  `jason: "groups"`
}

/*
GET
List the configured jobs
https://myserver.com:8080/jobs
    * limit - return x number of records
returns JSON:
[{
	"ID": int,
	"Command": "string",
	"JobName": "string",
	"Hour": "string",
	"Minute": "string",
	"Re3pMode": "string",
	"RepIter": "string
	"DayofWeek": "string",
	"IterinMonth": "string",
	"EndHour": "string",
	"EndMinute": "string",
	"MaxIter": "string",
	"DelayStart": "string",
	"Groups": "string"
}]
*/
func JobsListHandler(w http.ResponseWriter, r *http.Request){
    jobs := JobList(r)
    jsonbytes, err := json.Marshal(&jobs)
    chkErr(err)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)
}

func JobList(r *http.Request) []Job {
    var jobslist []Job
    querystr := "SELECT * from jobconfig"
    keys, ok := r.URL.Query()["limit"]
    if ok {
        querystr = fmt.Sprintf(querystr + " LIMIT %s", keys[0])
    }
    rows, err := db.Query(querystr)
    chkErr(err)
    defer rows.Close()
    for rows.Next() {
        var ID, Hour, Minute, RepMode, RepIter, DayofWeek, IterinMonth, EndHour, EndMinute, MaxIter, DelayStart, Command, JobName, Groups string
        err = rows.Scan(&ID, &Command, &JobName, &Hour, &Minute, &RepMode, &RepIter, &DayofWeek, &IterinMonth, &EndHour, &EndMinute, &MaxIter, &DelayStart, &Groups)
        chkErr(err)
        jobslist = append(jobslist, Job{ID, Command, JobName, Hour, Minute, RepMode, RepIter, DayofWeek, IterinMonth, EndHour, EndMinute, MaxIter, DelayStart, Groups})
    }
    return jobslist
}

/*
POST
Add jobs from JSON input
Required fields:
    * jobname
    * command
    * hour
    * minute
Other fields:
    * repminutes
    * rephours
    * repdays
    * repweeks
    * repmonths
    * dayofweek
    * iterinmonth
    * endhour
    * endminute
    * maxiter
    * delaystart
    * groups
https://myserver.com:8080/jobs/add
*/
func JobsAddHandler(w http.ResponseWriter, r *http.Request){
    var job Job
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    defer r.Body.Close()

    err = json.Unmarshal(body, &job)
    if err != nil {
        panic(err)
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(442)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }
    t := JobInsert(job)
    if t != nil {
        WebResult(w, http.StatusInternalServerError, "Failed to create job")
    } else {
        WebResult(w, http.StatusOK, "Job created")
    }
}

/*
GET
Delete a job based on its ID
https://myserver.com:8080/jobs/delete/{jobid}
*/
func JobsDeleteHandler(w http.ResponseWriter, r *http.Request){
    stmt, err := db.Prepare("DELETE FROM jobconfig WHERE id=?")
    chkErr(err)

    vars := mux.Vars(r)
    jobid := vars["jobid"]

    res, err := stmt.Exec(jobid)
    chkErr(err)
    aff, err := res.RowsAffected()
    chkErr(err)
    fmt.Fprintln(w, aff)
}

/*
POST
Edit a job based on its ID
https://myserver.com:8080/jobs/edit/
This takes the same JSON input and follows the same rules as JobsAddHandler
*/
func JobsEditHandler(w http.ResponseWriter, r *http.Request){
    var job Job
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    defer r.Body.Close()

    vars := mux.Vars(r)
    jobid := vars["jobid"]

    err = json.Unmarshal(body, &job)
    if err != nil {
        panic(err)
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(442)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }

    v := reflect.ValueOf(job)
    for i := 0; i < v.NumField(); i++ {
        if v.Type().Field(i).Name != "ID" && v.Field(i).String() != ""{
            setclause := fmt.Sprintf("%+v = '%+v' WHERE ID = %s", v.Type().Field(i).Name, v.Field(i), jobid)
            JobEdit(setclause)
        }
    }
}

/*
GET
Run a job with given ID
https://myserver.com:8080/jobs/run/{jobid}
    * type - specify if this should run a scheduled job or adhoc
        type scheduled will require the ID of the scheduled job
        type adhoc will require the ID of the scheduled job (default)
        https://myserver.com:8080/jobs/run/117?type=scheduled
*/
func JobsRunHandler(w http.ResponseWriter, r *http.Request) {
    systoken := GetSystemToken()
    table := schedtable
    vars := mux.Vars(r)
    jobid := vars["jobid"]

    jdetails := GetJobDetails(jobid, table)
    /*==================================
            PUT IN PROGRESS
    ==================================*/
    // Put the job in progress
    Progdate, Progtime := GetDateTime()
    progurl := fmt.Sprintf("https://%s:%d/jobs/progress/add?token=%s", localconn, bindport, systoken)
    progdata := Inprogress{jdetails.Id, jdetails.JobName, Progdate, Progtime}
    progjson, err := json.Marshal(progdata)
    chkErr(err)
    progreq, err := http.NewRequest("POST", progurl, bytes.NewBuffer(progjson))
    chkErr(err)
    progreq.Header.Set("Content-Type", "application/json")
    _, err = client.Do(progreq)
    chkErr(err)

    /*==================================
                RUN JOB
    ==================================*/
    // send job data to queue management
    q, err := qconn.QueueDeclare(jdetails.Groups, false, false, false, false, nil)
    chkErr(err)
    if qtype == "rabbitmq" {
        err = qconn.Publish("Job",
          q.Name,
          false,
          false,
          amqp.Publishing {
            ContentType: "text/plain",
            Body:        []byte(jdetails.Command),
          })
        chkErr(err)
    } else {
        fmt.Println(fmt.Sprintf("Unknown queue system %s\n", qtype))
    }
}

/*
Carry out the insert mysql query based on the contents of the Job struct
*/
func JobInsert(job Job) error {
    stmt, err := db.Prepare("INSERT INTO jobconfig" +
        "(Command, JobName, Hour, Minute, RepMode, RepIter, DayofWeek, IterinMonth, EndHour, EndMinute, MaxIter, DelayStart, Groups)" +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    chkErr(err)
    defer stmt.Close()
    _, err = stmt.Exec(&job.Command, &job.JobName, &job.Hour, &job.Minute, &job.RepMode, &job.RepIter, &job.DayofWeek, &job.IterinMonth, &job.EndHour, &job.EndMinute, &job.MaxIter, &job.DelayStart, &job.Groups)
    chkErr(err)
    return err
}

/*
Carry out the edit on the requested job
*/
func JobEdit(setclause string) error {
    stmt, err := db.Prepare("UPDATE jobconfig SET " + setclause)
    chkErr(err)
    _, err = stmt.Exec()
    chkErr(err)
    return err
}

// Get details for a given job
func GetJobDetails(jobid, table string) JobRun {
    var jobd JobRun
    querystr := fmt.Sprintf("SELECT ID, JobName, Command, Groups FROM %s WHERE ID='%s'", table, jobid)
    chkErr(err)
    rows, err := db.Query(querystr)
    chkErr(err)
    defer rows.Close()
    for rows.Next() {
        var ID, Command, JobName, Groups string
        err = rows.Scan(&ID, &JobName, &Command, &Groups)
        chkErr(err)
        jobd = JobRun{ID, JobName, Command, Groups}
    }

    return jobd
}
