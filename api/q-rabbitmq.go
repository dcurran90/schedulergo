package main

import (
    //"encoding/json"
    "fmt"

    "github.com/streadway/amqp"
)

type JobReturn struct {
    Id          string  `json: "id"`
    Name        string  `json: "name"`
    ExitStatus  int     `json: "exitstatus"`
    Message     string  `json: "message"`
    Errors      string  `json: "errors"`
}

func RabbitConnect() *amqp.Channel {
    conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%d", qserver, qport))
    chkErr(err)
    ch, err := conn.Channel()
    chkErr(err)
    //defer ch.Close()
    return ch
}



func RabbitConsume() <-chan amqp.Delivery {
    q, err := qconn.QueueDeclare("schedulergoadmin", false, false, false, false, nil)
    chkErr(err)
    msgs, err := qconn.Consume(q.Name, "", true, false, false, false, nil)
    chkErr(err)
    return msgs
}
