package main

import (
    "github.com/BurntSushi/toml"
)

type Mycnf struct {
    General     general
    Mysql       mysql
    Ui          ui
    Auth        auth
    Queue       queue
}

type general struct {
    Bind        string
    Port        int
    SSLCert     string
    SSLKey      string
}

type mysql struct {
    Server      string
    Port        int
    Username    string
    Password    string
    Database    string
}

type ui struct {
    WorkingDir  string
}

type auth struct {
    Type        string
}

type queue struct {
    Type        string
    Server      string
    Port        int
    Username    string
    Password    string
}

func ReadConf(configfile string) Mycnf {
    var config Mycnf
    _, err := toml.DecodeFile(configfile, &config)
    chkErr(err)
    return config
}
