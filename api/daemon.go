package main

import (
    "crypto/tls"
    "crypto/x509"
    "encoding/json"
    "database/sql"
    "flag"
    "fmt"
    "math/rand"
    "net/http"
    "os"
    "time"

    "github.com/streadway/amqp"
    "github.com/gorilla/mux"
    "github.com/gorilla/handlers"
    "github.com/rakyll/statik/fs"
    _ "github.com/go-sql-driver/mysql"
    _ "../statik"
)

type WebReturn struct {
    Status int      `json: "status"`
    Message string  `json: "message"`
}

const (
    version = 0.2
    schedtable = "scheduled_jobs"
    conftable = "jobconfig"
    progtable = "inprogress"
    comptable = "completed_jobs"
    sesstable = "sessions"
    usertable = "usertable"
    localconn = "127.0.0.1"
    jobcachetable = "job_cache"
)

var (
    uname string
    passwd string
    myserver string
    myport int
    dbase string
    bindserver string
    bindport  int
    sslcert string
    sslkey string
    qtype string
    qserver string
    qport int
    quser string
    qpass string
    sesscookie = "schedgo_login"
    client *http.Client
    db *sql.DB
    err error
    pool *x509.CertPool
    qconn *amqp.Channel
)

// function to take care of error checking
func chkErr(err error) {
    if err != nil {
        panic(err)
    }
}

// function to write errors to web response
func webErr(w http.ResponseWriter, err error) {
    if err != nil {
        WebResult(w, http.StatusInternalServerError, "There was an error")
    }
}

func Install() {
    InstallDb()
    CreateSystemUser()
	  os.Exit(0)
}

func init() {
    // set up new certificate pool using certs.go
    pool = x509.NewCertPool()
    pool.AppendCertsFromPEM(pemCerts)
    client = &http.Client{
        Transport: &http.Transport{
            TLSClientConfig: &tls.Config{RootCAs: pool},
        },
        Timeout: 10,
    }
    if bindport == 0 {
        bindport = 8080
    }
}

func main() {
    installflag := flag.Bool("install", false, "Tell schedulergo to install necessary dependencies")
    versionflag := flag.Bool("version", false, "Show version and exit")
    configfile := flag.String("conf", "/etc/schedulergo/conf.toml", "location to conf.toml")
    flag.Parse()
    if *versionflag == true {
        fmt.Println(version)
        os.Exit(0)
    }
    conf := ReadConf(*configfile)
    uname = conf.Mysql.Username
    passwd = conf.Mysql.Password
    myserver = conf.Mysql.Server
    myport = conf.Mysql.Port
    dbase = conf.Mysql.Database
    bindserver = conf.General.Bind
    bindport = conf.General.Port
    sslcert = conf.General.SSLCert
    sslkey = conf.General.SSLKey
    qtype = conf.Queue.Type
    qserver = conf.Queue.Server
    qport = conf.Queue.Port
    quser = conf.Queue.Username
    qpass = conf.Queue.Password

    db = ConnectDatabase()
    defer db.Close()

    if *installflag == true {
        Install()
    }
    if myserver == "" {
        myserver = "127.0.0.1"
        myport = 3306
    }
    if bindserver == "" {
        bindserver = "127.0.0.1"
    }
    if bindport == 0 {
        bindport = 8080
    }

    tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
    }
    client = &http.Client{Transport: tr}

    if qtype == "rabbitmq" {
        qconn = RabbitConnect()
    } else {
        fmt.Println(fmt.Sprintf("Unknown queue system '%s'\n", qtype))
    }

    statikFs, err := fs.New()
    chkErr(err)

    go func() {
        /*
        Makes connection to Message queue system and keeps
        the connection open, waiting for return messages
        */
        var msgs <-chan amqp.Delivery
        if qtype == "rabbitmq" {
            for {
                msgs = RabbitConsume()
                MarkComplete(msgs)
            }
        } else {
            fmt.Println(fmt.Sprintf("Unknown queue system '%s'\n", qtype))
        }
    }()

    go func() {
        /*
        Starts the process of waiting to run jobs
        */
        jobcache, err := BuildCache()
        chkErr(err)
        CheckRun(jobcache)
    }()

    r := mux.NewRouter()

    r.HandleFunc("/auth/login", AuthHandler).Methods("POST")
    r.HandleFunc("/auth/logout", AuthLogout)
    r.HandleFunc("/cache/build", BuildCacheHandler).Methods("POST")
    //r.HandleFunc("/agents", AgentsHandler)
    //r.HandleFunc("/agents/edit", AgentsEditHandler).Methods("POST")
    //r.HandleFunc("/agents/delete", AgentsDelHandler)
    //r.HandleFunc("/agents/register", AgentsAddHandler).Methods("POST")
    r.HandleFunc("/jobs", JobsListHandler)
    r.HandleFunc("/jobs/add", JobsAddHandler).Methods("POST")
    r.HandleFunc("/jobs/edit/{jobid}", JobsEditHandler).Methods("POST")
    r.HandleFunc("/jobs/delete/{jobid}", JobsDeleteHandler)
    r.HandleFunc("/jobs/progress", JobsProgressHandler)
    r.HandleFunc("/jobs/progress/add", JobsProgressAddHandler).Methods("POST")
    r.HandleFunc("/jobs/progress/delete/{jobid}", JobsProgressDelHandler)
    r.HandleFunc("/jobs/complete", JobsCompletedHandler)
    r.HandleFunc("/jobs/complete/add", JobsCompletedAddHandler).Methods("POST")
    r.HandleFunc("/jobs/complete/delete/{jobid}", JobsCompletedDelHandler)
    r.HandleFunc("/jobs/run/{jobid}", JobsRunHandler).Methods("GET")
    r.HandleFunc("/jobs/scheduled", JobsScheduledHandler)
    r.HandleFunc("/scheduler", SchedulerJobsHandler).Methods("POST")
    r.HandleFunc("/scheduler/{jobid}", SchedulerJobsHandler).Methods("POST")
    r.HandleFunc("/scheduler/delete/{jobid}", SchedulerJobsDelHandler)
    r.HandleFunc("/status", statushandler)
    r.HandleFunc("/users", UsersHandler)
    r.HandleFunc("/users/add", UsersAddHandler).Methods("POST")
    r.HandleFunc("/users/delete/{userid}", UsersDelHandler)
    r.PathPrefix("/").Handler(http.FileServer(statikFs))
    r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(statikFs)))

    err = http.ListenAndServeTLS(fmt.Sprintf("%s:%d", bindserver, bindport), sslcert, sslkey, checker{handlers.LoggingHandler(os.Stdout, r)})
    chkErr(err)
}

/*
GET
https://myserver.com:8080/status
Gives OK if the API is running
Returns JSON:
{
    "Status":"OK",
    "Message":"API is running"
}
*/
func statushandler(w http.ResponseWriter, r *http.Request){
    WebResult(w, http.StatusOK, "API is running")
}

/*
Generate a random string of letters and numbers
Primarily used to create the AuthToken for a user
Returns string
*/
func randStr(n int) string {
    rand.Seed(time.Now().Unix())
    const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
    b := make([]byte, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

/*
Generic way to return JSON data
*/
func WebResult(w http.ResponseWriter, status int, msg string) {
    var ret WebReturn
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(status)
    ret.Status = status
    ret.Message = msg
    jsonbytes, err := json.Marshal(&ret)
    chkErr(err)
    w.Write(jsonbytes)
}
/*
Create a system user to be used by automatic processes API calls
*/
func CreateSystemUser() {
    plainpass := randStr(12)
    pass, err := HashPassword(plainpass)
    chkErr(err)
    user := User{0, "system", "user", "system", pass, ""}
    fmt.Println(fmt.Sprintf("Creating system user with password %s", plainpass))
    UserInsert(user)
}

/*
Work out current time/date
*/
func GetDateTime() (string, string) {
    now := time.Now()
    Date := now.Format("2006-01-02")
    Time := now.Format("15:04:05")
    /*year, month, day := now.Date()
    hour, mins, secs := now.Clock()
    Date := fmt.Sprintf("%d-%d-%d", year, month, day) // YYYY-MM-DD
    Time := fmt.Sprintf("%d:%02d:%02d", hour, mins, secs) // HH-MM-SS*/
    return Date, Time
}
