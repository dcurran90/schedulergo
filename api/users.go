package main

import (
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "net/http"

    "github.com/gorilla/mux"
    _ "github.com/go-sql-driver/mysql"
)

type User struct {
    ID int              `json: "id"`
    FirstName string    `json: "firstname"`
    SecondName string   `json: "secondname"`
    Username string     `json: "username"`
    Password string     `json: "password"`
    AuthToken string    `json: "authtoken"`
}

/*
GET
List the configured users.
https://myserver.com:8080/users
returns JSON:
[{
    "id": int,
    "firstname": "string",
    "secondname": "string",
    "username": "string",
    "password": "string",
    "authtoken":"string"
}]
*/
func UsersHandler(w http.ResponseWriter, r *http.Request) {
    var userslist []User
    keys, ok := r.URL.Query()["limit"]
    querystr := "SELECT * from usertable"
    if ok {
        querystr = fmt.Sprintf(querystr + " LIMIT %s", keys[0])
    }
    rows, err := db.Query(querystr)
    chkErr(err)
    defer rows.Close()
    for rows.Next() {
        var ID int
        var FirstName, SecondName, Username, Password, AuthToken string
        err = rows.Scan(&ID, &FirstName, &SecondName, &Username, &Password, &AuthToken)
        userslist = append(userslist, User{ID, FirstName, SecondName, Username, Password, AuthToken})
        chkErr(err)
    }
    jsonbytes, err :=json.Marshal(&userslist)
    chkErr(err)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)
}

/*
POST
Add a user to the usertable table
https://myserver.com:8080/users/add
Requires the following fields:
    * FirstName
    * SecondName
    * Username
    * Password
    ID and AuthToken will be overwritten if passed
*/
func UsersAddHandler(w http.ResponseWriter, r *http.Request) {
    var user User
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    err = r.Body.Close()
    chkErr(err)
    err = json.Unmarshal(body, &user)
    if err != nil {
        panic(err)
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(442)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }
    user.AuthToken = randStr(25)
    user.Password, err = HashPassword(user.Password)
    chkErr(err)
    u := UserInsert(user)
    if u != nil {
        WebResult(w, http.StatusInternalServerError, "Failed to create user")
    } else {
        WebResult(w, http.StatusOK, "User created")
    }
}

/*
GET
Delete a user based on their ID
https://myserver.com:8080/users/delete/{userid}
*/
func UsersDelHandler(w http.ResponseWriter, r *http.Request) {
    var username string
    vars := mux.Vars(r)
    userid := vars["userid"]
    row := db.QueryRow(fmt.Sprintf("SELECT user FOMR usertable WHERE ID=%s", userid))
    err := row.Scan(&username)

    stmt, err := db.Prepare("DELETE FROM usertable WHERE ID=?")
    chkErr(err)
    _, err = stmt.Exec(userid)
    chkErr(err)

    sess, err := db.Prepare("DELETE FROM sessions WHERE User=?")
    chkErr(err)
    _, err = sess.Exec(username)
    chkErr(err)
}

func UserInsert(user User) error {
    stmt, err := db.Prepare("INSERT INTO usertable" +
        "(FirstName, SecondName, Username, Password, AuthToken)" +
        "VALUES (?, ?, ?, ?, ?)")
    chkErr(err)
    defer stmt.Close()
    _, err = stmt.Exec(&user.FirstName, &user.SecondName, &user.Username, &user.Password, &user.AuthToken)
    chkErr(err)
    return err
}
