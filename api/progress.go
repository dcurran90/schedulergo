package main

import (
    "encoding/json"
    "fmt"
    "net/http"
    "io"
    "io/ioutil"


    _ "github.com/go-sql-driver/mysql"
    "github.com/gorilla/mux"
)

type Inprogress struct {
    ID      string  `json: "id"`
    JobName string  `json: "jobname"`
    Date    string  `json: "date"`
    Time    string  `json: "time"`
}

/*
GET
List jobs that are in progess
https://myserver.com:8080/jobs/progress
Returns JSON:
[{
	"id": int,
	"jobname": "string",
	"date": "string" (YYYY/MM/DD),
	"time": "string", (HH:MM:SS)
}]
*/
func JobsProgressHandler(w http.ResponseWriter, r *http.Request){
    rows, err := db.Query("SELECT * from inprogress")
    chkErr(err)
    var jobslist []Inprogress
    for rows.Next() {
        var ID, JobName, Time, Date string
        err = rows.Scan(&ID, &JobName, &Date, &Time)
        jobslist = append(jobslist, Inprogress{ID, JobName, Date, Time})
        chkErr(err)
    }
    jsonbytes, err :=json.Marshal(&jobslist)
    chkErr(err)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)
}

/*
POST
Add a job to the inprogress table
https://myserver.com:8080/jobs/progress/add
Fields (all required):
    * id
    * jobname
    * date
    * time
*/
func JobsProgressAddHandler(w http.ResponseWriter, r *http.Request){
    var progess Inprogress
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    err = r.Body.Close()
    chkErr(err)

    err = json.Unmarshal(body, &progess)
    if err != nil {
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(442)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }
    // Remove from scheduled_jobs first
    schedurl := fmt.Sprintf("https://%s:%d/scheduler/delete/%s?token=%s", bindserver, bindport, progess.ID, GetSystemToken())
    _, err = client.Get(schedurl)
    chkErr(err)
    t := ProgInsert(progess)
    if t != nil {
        WebResult(w, http.StatusInternalServerError, "Failed to move job to in progress")
    } else {
        WebResult(w, http.StatusOK, "Job in progress")
    }
}

/*
GET
Remove a job from the inprogress table
https://myserver.com:8080/jobs/progress/delete/{jobid}
*/
func JobsProgressDelHandler(w http.ResponseWriter, r *http.Request){
    stmt, err := db.Prepare(fmt.Sprintf("DELETE FROM %s WHERE ID=?", progtable))
    chkErr(err)

    vars := mux.Vars(r)
    jobid := vars["jobid"]

    res, err := stmt.Exec(jobid)
    chkErr(err)
    aff, err := res.RowsAffected()
    chkErr(err)
    fmt.Fprintln(w, aff)
}

/*
Run the insert query to add the job to inprogress
*/
func ProgInsert(job Inprogress) error {
    stmt, err := db.Prepare("INSERT INTO inprogress" +
        "(ID, JobName, Date, Time)" +
        "VALUES (?, ?, ?, ?)")
    chkErr(err)
    defer stmt.Close()
    _, err = stmt.Exec(&job.ID, &job.JobName, &job.Date, &job.Time)
    chkErr(err)
    return err
}
