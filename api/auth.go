package main

import (
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "net/http"
    "strings"
    "time"

    "golang.org/x/crypto/bcrypt"
    _ "github.com/go-sql-driver/mysql"
)

type Auth struct {
    Username string `json: "username"`
    Password string `json: "password"`
}

type Session struct {
    ID int
    User string
    UserKey string
    Valid bool
    Expires time.Time
    Since time.Time
}

type checker struct {
    h http.Handler
}

/*
POST
Authentication method for the UI
https://myserver.com:8080/auth/login
This will take the useranme and password submitted via the login form
check the user exists in the database then
compare the stored hash against the given password
*/
func AuthHandler(w http.ResponseWriter, r *http.Request) {
    var auth Auth
    var hash string
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    err = r.Body.Close()
    chkErr(err)
    err = json.Unmarshal(body, &auth)
    if err != nil {
        panic(err)
        w.Header().Set("Content-Type", "applicatiion.json; charset=UTF-8")
        w.WriteHeader(http.StatusUnauthorized)
        err = json.NewEncoder(w).Encode(err)
        chkErr(err)
    }
    querystr := fmt.Sprintf("SELECT Password FROM usertable WHERE Username='%s'", auth.Username)
    row:= db.QueryRow(querystr)
    err = row.Scan(&hash)
    if err != nil {
        WebResult(w, http.StatusUnauthorized, "FAIL")
    } else {
        match := CheckPasswordHash(auth.Password, hash)
        if match == true {
            SetSessionCookie(w, r, auth.Username)
            WebResult(w, http.StatusOK, "OK")
        } else {
            WebResult(w, http.StatusUnauthorized, "FAIL")
        }
    }
}

/*
Log out of the current session
this will delete all session keys for the user
*/
func AuthLogout(w http.ResponseWriter, r *http.Request) {
    var user string
    userkey := GetCookie(r)
    userquer := fmt.Sprintf("SELECT User FROM sessions WHERE UserKey='%s'", userkey)
    urow := db.QueryRow(userquer)
    err := urow.Scan(&user)
    chkErr(err)
    stmt, err := db.Prepare("DELETE FROM sessions WHERE User=?")
    chkErr(err)
    _, err = stmt.Exec(user)
    if err != nil {
        WebResult(w, http.StatusInternalServerError, "Unable to log out")
        return
    }
    WebResult(w, http.StatusOK, "OK")
}

/*
Hash the password using bcrypt
taken from townsourced.com
*/
func HashPassword(password string) (string, error) {
    bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
    return string(bytes), err
}

/*
Compare the has against the password sent from user
taken from townsourced.com
*/
func CheckPasswordHash(password, hash string) bool {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
    return err == nil
}

/*
Write the user's login session to a database
This can then be checked by CheckSession()
adapted from townsourced.com
*/
func NewSession(user string) (*Session, error) {
    s := &Session{
        User:       user,
        UserKey:    randStr(128),
        Valid:      true,
        Expires:    time.Now().AddDate(0, 0, 3),
        Since:      time.Now(),
    }
    stmt, err := db.Prepare("INSERT INTO sessions" +
        "(User, UserKey, Valid, Expires, Since)" +
        "VALUES (?, ?, ?, ?, ?)")
    if err != nil {
        return nil, err
    }
    defer stmt.Close()
    _, err = stmt.Exec(&s.User, &s.UserKey, &s.Valid, &s.Expires, &s.Since)
    chkErr(err)
    return s, nil
}

/*
Set the cookie in the user's session
This will be checked by CheckSession()
*/
func SetSessionCookie(w http.ResponseWriter, r *http.Request, user string) error {
    s, err := NewSession(user)
    if err != nil {
        return err
    }
    cookie := &http.Cookie{
        Name:       sesscookie,
        Value:      s.UserKey,
        Path:       "/",
        Expires:    s.Expires,
    }
    http.SetCookie(w, cookie)
    return nil
}


/*
Retrives cookies from the users browser and returns the value of
sesscookie
*/
func GetCookie(r *http.Request) string {
    cookies:= r.Cookies()
    cValue := ""
    for i := range cookies {
        if cookies[i].Name == sesscookie {
            if cookies[i].Value != "" {
                cValue = cookies[i].Value
            }
        }
    }
    return cValue
}

/*
Get sessions/API information for a user
check that against the sessions table
ignore this for /login.html and /static content
else users would never be able to see the Login screen
*/
func (c checker) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    // Ignore and show login page if hitting login.html, status or any static files
    if strings.Contains(r.URL.Path, "login") || strings.Contains(r.URL.Path, "static") || strings.Contains(r.URL.Path, "status") || strings.Contains(r.URL.Path, "register") {
        c.h.ServeHTTP(w, r)
        return
    }

    // If we're dealing with an API request then we don't need cookies
    // Instead we get the ?token= parameter from the URL
    keys, ok := r.URL.Query()["token"]
    if ok {
        err := CheckToken(keys[0])
        if err != nil {
            http.Error(w, "Invalid or no API key", http.StatusUnauthorized)
            return
        }
        c.h.ServeHTTP(w, r)
        return
    }
    // Get all the cookies from user
    // iterate over them to find the relevant one
    cValue := GetCookie(r)

    // give error if there is no cookie value
    if cValue == "" {
        if r.URL.Path == "/" || strings.Contains(r.URL.Path, "index.html") {
            http.Redirect(w, r, "/login.html", http.StatusFound)
        } else {
            http.Error(w, "Invalid or no session key", http.StatusUnauthorized)
        }
        return
    }

    // Get details of saved sessions with the specified userkey
    // If there is no error then carry on with the request
    // else return invalid key error
    sess := GetSession(cValue)
    if sess != "" {
        if r.URL.Path == "/" || strings.Contains(r.URL.Path, "index.html") {
            http.Redirect(w, r, "/login.html", http.StatusFound)
        } else {
            http.Error(w, "Invalid or no session key", http.StatusUnauthorized)
        }
        return
    }
    c.h.ServeHTTP(w, r)
}

/*
Get sessions information from the database
then check that the session is both valid and not expired
*/
func GetSession(userkey string) string {
    var expires time.Time
    var valid bool
    querystr := fmt.Sprintf("SELECT Expires, Valid FROM sessions WHERE UserKey='%s'", userkey)
    row := db.QueryRow(querystr)
    err := row.Scan(&expires, &valid)
    if err != nil {
        return "FAIL"
    }
    if !valid || expires.Before(time.Now()) {
        return "FAIL"
    }
    return ""
}

/*
Checks a user token to make sure they are authorised to use the api
*/
func CheckToken(token string) error {
    var authtoken string
    if token == "" {
        var err error
        return err
    }
    querystr := fmt.Sprintf("SELECT AuthToken FROM usertable WHERE AuthToken='%s'", token)
    row := db.QueryRow(querystr)
    err := row.Scan(&authtoken)
    if err != nil {
        return err
    }
    return nil
}

/*
Get a user token based on their session cookie
*/
func GetSystemToken() string {
    var token string
    querystr := fmt.Sprintf("SELECT AuthToken FROM usertable WHERE Username='system'")
    row := db.QueryRow(querystr)
    err := row.Scan(&token)
    chkErr(err)
    return token
}
