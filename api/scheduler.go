package main

import (
    //"encoding/json"
    "fmt"
    "net/http"
    "time"
    "strconv"
    "strings"

    "github.com/gorilla/mux"
)

type Jobresp struct {
    ID int `json:"id"`
}

func SchedulerJobsHandler(w http.ResponseWriter, r *http.Request) {
    var jobs []Job
    jobs = JobList(r)

    for _, v := range jobs {
        NextSched(v)
    }
}

func SchedulerJobsDelHandler(w http.ResponseWriter, r *http.Request) {
    stmt, err := db.Prepare(fmt.Sprintf("DELETE FROM %s WHERE ID=?", schedtable))
    chkErr(err)

    vars := mux.Vars(r)
    jobid := vars["jobid"]

    res, err := stmt.Exec(jobid)
    chkErr(err)
    aff, err := res.RowsAffected()
    chkErr(err)
    fmt.Fprintln(w, aff)
}

func GetLastSched(job Job) {
    query := fmt.Sprintf("SELECT * FROM jobconfig WHERE JobName = '%s'", job.JobName)
    fmt.Println(query)
}

func NextSched(job Job) {
    // Get the current time and split that into its component parts
    // year, month, day, hour, minute, second
    // so that we can manipulate them based on the given values
    now := time.Now()
    twomonth := now.AddDate(0, 2, 0) // Get the date for two months in the future
    year, month, day := now.Date()
    hour, minute, second := now.Clock()
    // now change the values based on jobconfig data
    if (job.Hour != "") {
        hour, err = strconv.Atoi(job.Hour)
        chkErr(err)
    }
    if (job.Minute != "") {
        minute, err = strconv.Atoi(job.Minute)
        chkErr(err)
    }
    // Change the time of now to set the first time to schedule the job
    now = time.Date(year, month, day, hour, minute, second, 0, time.UTC)
    fmt.Println(fmt.Sprintf("%s-%s-%s: %s:%s:%s", year, month, day, hour, minute, second))
    ScheduleJob(now, &job)

    // Now loop over and schedule jobs for the next two months
    maxiter, err := strconv.Atoi(job.MaxIter)
    maxbool := false
    if maxiter > 0 {
        maxbool = true
    }
    chkErr(err)
    for c := 0; now.Before(twomonth); c++ {
        if maxbool == true {
            if c == maxiter - 1 {
                break
            }
        }
        repeat := job.RepMode
        iter, err := strconv.Atoi(job.RepIter)
        chkErr(err)
        if (job.DelayStart != "") {
            delay, err := strconv.Atoi(job.DelayStart)
            chkErr(err)
            now = now.AddDate(0, 0, delay)
        }
        if (repeat == "mins") {//min
            now = now.Add(time.Minute * time.Duration(iter))
        } else if (repeat == "hours") {//hour
            now = now.Add(time.Hour * time.Duration(iter))
        } else if (repeat == "days") {//day
            now = now.AddDate(0, 0, iter)
        } else if (repeat == "weeks") {//week
            now = now.AddDate(0, 0, 7 * iter)
        } else if (repeat == "months") {//month
            now = now.AddDate(0, iter, 0)
        } else {
            now = now.AddDate(0, 0, 1)
        }
        fmt.Println(now.String())

        ScheduleJob(now, &job)
    }
}

func GenerateSchedule(job Job, now time.Time) {

}

func ScheduleJob(sched time.Time, job *Job) {
    // sched comes in as 2018-10-06 01:10:50 +0000 UTC
    // so we splt it based on spaces
    // and get date and time from it
    schedstr := strings.Fields(sched.String())
    schedate := schedstr[0]
    schedtime := schedstr[1]
    chkquer, err := db.Prepare(fmt.Sprintf("SELECT * FROM %s WHERE JobName=? AND Time=? AND Date=?", schedtable))
    chkErr(err)
    chkres, err := chkquer.Exec(job.JobName, schedtime, schedate)
    chkrows, err := chkres.RowsAffected()
    if chkrows == 0 {
        insquer, err := db.Prepare(fmt.Sprintf("INSERT INTO %s (JobName, Time, Date, Command, Groups) VALUES (?, ?, ?, ?, ?)", schedtable))
        chkErr(err)
        res, err := insquer.Exec(job.JobName, schedtime, schedate, job.Command, job.Groups)
        chkErr(err)
        _, err = res.RowsAffected()
        chkErr(err)
    }
}
