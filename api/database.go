package main

import (
    "fmt"
    "database/sql"

    _ "github.com/go-sql-driver/mysql"
)

func InstallDb() {
    //CreateDatabase()
    UserTable()
    JobConfigTable()
    ScheduledJobsTable()
    ProgressTable()
    CompletedJobsTable()
    SessionsTable()
    CacheTable()
}

func ConnectDatabase() *sql.DB {
    db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", uname, passwd, myserver, myport, dbase))
    chkErr(err)
    err = db.Ping()
    chkErr(err)
    return db
}

/*func CreateDatabase() error {
    stmt, err := db.Prepare("CREATE DATABASE ?")
    chkErr(err)
    defer stmt.Close()
    _, err = stmt.Exec(dbase)
    chkErr(err)
    return err
}*/

func CreateTable(table, cols string) error {
    //fmt.Println(fmt.Sprintf("%s : \n\t%s", table, cols))
    querystr := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (%s)", table, cols)
    _, err := db.Query(querystr)
    /*quer, err := db.Prepare("CREATE TABLE IF NOT EXISTS ? (?)")
    chkErr(err)
    defer quer.Close()
    _, err = quer.Exec(table, cols)
    chkErr(err)
    err = db.commit()*/
    chkErr(err)
    return err
}

func UserTable() {
    stmt := `ID int(11) NOT NULL AUTO_INCREMENT,
    FirstName varchar(255) NOT NULL,
    SecondName varchar(255) NOT NULL,
    Username varchar(255) NOT NULL,
    Password varchar(255) NOT NULL,
    AuthToken varchar(255) NOT NULL,
    PRIMARY KEY (ID)`
    CreateTable(usertable, stmt)
}

func JobConfigTable() {
    stmt := `ID int(11) NOT NULL AUTO_INCREMENT,
    Command TEXT NOT NULL,
    JobName varchar(180) NOT NULL,
    Hour varchar(2) NOT NULL,
    Minute varchar(2) NOT NULL,
    RepMode varchar(10) NOT NULL,
    RepIter varchar (5) NOT NULL,
    DayofWeek varchar(1) NOT NULL,
    IterinMonth varchar(1) NOT NULL,
    EndHour varchar(2) NOT NULL DEFAULT 0,
    EndMinute varchar(2) NOT NULL DEFAULT 0,
    MaxIter varchar(10) NOT NULL DEFAULT 0,
    DelayStart varchar(10) NOT NULL DEFAULT 0,
    Groups varchar(25) NOT NULL DEFAULT 'General',
    PRIMARY KEY (ID),
    UNIQUE KEY ix_jobname (JobName)`
    CreateTable(conftable, stmt)
}

func ScheduledJobsTable() {
    stmt := `ID int NOT NULL AUTO_INCREMENT,
    JobName VARCHAR(255) NOT NULL,
    Time time NOT NULL,
    Date date NOT NULL,
    Command text NOT NULL,
    JobStatus int NOT NULL DEFAULT 1,
    Groups varchar(25) NOT NULL DEFAULT 'general',
    PRIMARY KEY (ID)`
    CreateTable(schedtable, stmt)
}

func CompletedJobsTable() {
    stmt := `ID int NOT NULL AUTO_INCREMENT,
    JobID INT NOT NULL,
    JobName VARCHAR(255) NOT NULL,
    Time time NOT NULL,
    Date date NOT NULL,
    ExitStatus int,
    PRIMARY KEY (ID)`
    CreateTable(comptable, stmt)
}

func ProgressTable() {
    stmt := `ID INT NOT NULL,
    JobName VARCHAR(255) NOT NULL,
    Date date NOT NULL,
    Time time NOT NULL,
    PRIMARY KEY(ID)`
    CreateTable(progtable, stmt)
}

func SessionsTable() {
    stmt := `ID INT NOT NULL AUTO_INCREMENT,
    User VARCHAR(255) NOT NULL,
    UserKey VARCHAR(255) NOT NULL,
    Valid TINYINT NOT NULL,
    Expires DATETIME NOT NULL,
    Since DATETIME NOT NULL,
    PRIMARY KEY(ID)`
    CreateTable(sesstable, stmt)
}

func CacheTable() {
    stmt := `Refresh TINYINT,
    Built DATETIME`
    CreateTable(jobcachetable, stmt)
    date, time := GetDateTime()
    datetime := fmt.Sprintf("%s %s", date, time)
    firstcache, err := db.Prepare("INSERT INTO job_cache (Refresh, Built) VALUES ('1', ?)")
    defer firstcache.Close()
    chkErr(err)
    _, err = firstcache.Exec(datetime)
    chkErr(err)
}
